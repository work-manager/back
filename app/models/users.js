module.exports = function(sequelize, DataTypes) {
	return sequelize.define('users', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: false,
			validate: {
				is: /(^([A-Z][a-z]){1}[a-z]{1,}$)/,
			}
		},
		surname: {
			type: DataTypes.STRING(45),
			allowNull: true,
			validate: {
				is: /(^([A-Z][a-z]){1}[a-z]{1,}$)/,
			}
		},
		email: {
			type: DataTypes.STRING(45),
			allowNull: false,
			validate: {
				isEmail: true,
			}
		},
		pass: {
			type: DataTypes.STRING(255),
			allowNull: false,
			validate: {
				is: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}/g,
			}
		},
		logo: {
			type: DataTypes.TEXT,
			allowNull: true,
			validate: {
				isUrl: true, 
			}
		}
	}, {
		tableName: 'users'
	});
};
