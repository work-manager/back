module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tasks', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true
		},
		user_id: {
			type: DataTypes.INTEGER(11),
			allowNull: true,
			references: {
				model: 'users',
				key: 'id'
			}
		},
		column_id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'columns',
				key: 'id'
			}
		},
		priority: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		title: {
			type: DataTypes.STRING(45),
			allowNull: false
		},
		description: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		estimation: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		},
		time_spent: {
			type: DataTypes.INTEGER(11),
			allowNull: true
		}
	}, {
		tableName: 'tasks'
	});
};
