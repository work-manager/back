module.exports = function(sequelize, DataTypes) {
	return sequelize.define('u_b', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true
		},
		user_id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'users',
				key: 'id'
			}
		},
		board_id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'boards',
				key: 'id'
			}
		},
		security: {
			type: DataTypes.STRING(45),
			allowNull: false
		}
	}, {
		tableName: 'u_b'
	});
};
