module.exports = function(sequelize, DataTypes) {
	return sequelize.define('columns', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true
		},
		board_id: {
			type: DataTypes.INTEGER(11),
			allowNull: false,
			references: {
				model: 'boards',
				key: 'id'
			}
		},
		title: {
			type: DataTypes.STRING(45),
			allowNull: true
		},
		color: {
			type: DataTypes.STRING(45),
			allowNull: true
		}
	}, {
		tableName: 'columns'
	});
};
