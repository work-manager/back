module.exports = function(sequelize, DataTypes) {
	return sequelize.define('boards', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true
		},
		title: {
			type: DataTypes.STRING(45),
			allowNull: false
		},
		description: {
			type: DataTypes.STRING(45),
			allowNull: true
		}
	}, {
		tableName: 'boards'
	});
};
