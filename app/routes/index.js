const express = require('express');
const apiRouter = express.Router();
const mainRouter = express.Router();

const mainController = require('./../controllers/main');


mainRouter.use('*', mainController.getIndexPage);
apiRouter.use('*', mainController.getIndexPage);


module.exports = (app) => {
  app.use('/api/', apiRouter);
  app.use('/', mainRouter);

}
