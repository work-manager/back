const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const config = require('./config/' + (process.env.NODE_ENV || 'dev') + '.js');

const port = normalizePort(process.env.PORT || config.port);

const app = express();
require('./app/routes')(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, './app/public')));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "access-control-allow-origin, Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
  next();
});

app.listen(port);

config.checkMySQLConnection()
  .then(() => {
    console.log('Connection has been established successfully.');
    config.sequelize.sync()
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
    process.exit(1);
  });


function normalizePort(val) {
  const port = parseInt(val, 10);

  if (!isNaN(port) && port >= 0) {
    return port;
  }
  return false;
}