const Sequelize = require('sequelize');

const port = 3001;
const secret = 'workmanager';

const sequelize = new Sequelize(
    'workmanager',  // database
    'root',         // user
    'root',         // pass
    {
        host: 'localhost',
        dialect: 'mysql',
        define: {
            timestamps: false
        }
    }
);

const checkMySQLConnection = () => {
    return sequelize
        .authenticate()
}
module.exports = {
    checkMySQLConnection,
    sequelize,
    port,
    secret,
}
